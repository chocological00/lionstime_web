<?php

include 'util.php';
include 'sql.php';

//terminate if not called by server itself
//possible issue(which is not likely going to happen)
//@see comment on https://gitlab.com/hohs/lionstime_web/issues/12 for explanation
if($_SERVER['SERVER_ADDR'] != $_SERVER['REMOTE_ADDR'])
{
	echo "Must be called from server itself.";
	exit();
}

//if open is set $val is 1
$val = 0;
if(isset($_GET['open']))
{
	$val = 1;
	$conn->query("TRUNCATE `signups`");
}

// set Parameter enables to val and print success
setParam("enabled", $val);
echo "Success."; //WHEN CHANGING THIS LINE, DO NOT FORGET TO UPDATE MAINTENANCE SCRIPT ACCORDINGLY! THE SERVER WILL INFINITE LOOP OTHERWISE!

?>