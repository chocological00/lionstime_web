<?php
include_once 'util.php';
include_once 'sql.php';
$row = 1;
$start = isset($_GET['start']) ? intval($_GET['start']) : 1;
$end = isset($_GET['end']) ? intval($_GET['end']) : 20;

if (($handle = fopen("user-spreadsheet.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
	if($row >= $start) {
		$num = count($data);
		if($data[1] != 'Username') {
			$username = $data[1];
			$firstName = $data[6];
			$lastName = $data[5];
			$gradYear = $data[8];
			preg_match_all('!\d+!', $username, $preg);
			$password = $preg[0][0]."-$gradYear";
			$hash = password_hash($password, PASSWORD_DEFAULT);
			echo "$username<br>$firstName<br>$lastName<br>$gradYear<br>$password<br>$hash<br><br>";

			$conn->query("INSERT INTO `users` (`username`, `hash`, `firstName`, `lastName`, `gradYear`, `teacher`) VALUES ('$username', '$hash', '$firstName', '$lastName', $gradYear, 0)");
		}
	}
	if($row >= $end) break;
	$row++;
}
    fclose($handle);
}

?>

<script>

setTimeout(function() {
	window.location = "<?php echo 'addusers.php?start='.($end + 1).'&end='.($end + 20); ?>";
}, 100);

</script>