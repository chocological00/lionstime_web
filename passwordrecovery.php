<?php

// If form has been submitted
if(isset($_POST['submit'])) {
	
	include_once 'util.php';
	include_once 'sql.php';
	
	// If any fields are missing
	if(!isset($_POST['username']) or !isset($_POST['gradyear']) or !isset($_POST['firstname']) or !isset($_POST['lastname']))
		redirError("./passwordrecovery.php", "Invalid query parameters.");
	
	$username = $conn->real_escape_string($_POST['username']);
	$gradyear = $conn->real_escape_string($_POST['gradyear']);
	$firstname = $conn->real_escape_string($_POST['firstname']);
	$lastname = $conn->real_escape_string($_POST['lastname']);
	
	// If any fields are blank
	if($username == "" or $gradyear == "" or $firstname == "" or $lastname == "")
		redirError("./passwordrecovery.php", "All fields must be filled in.");

	$query = $conn->query("SELECT * FROM `users` WHERE `username`='$username'");
	
	// If user does not exist
	if($query->num_rows < 1)
		redirError("./passwordrecovery.php", "That username does not exist.");
	
	$result = $query->fetch_assoc();
	
	//teacher verifycation - Since this can be used in trolling, teacher password reset should be blocked.
	$isTeacher = $result['teacher'] > 0;
	if($isTeacher)
		redirError("./login.php", "Please contact to Mr. Zaron if you are a teacher.");
	
	// If any information given is incorrect
	if(!( ($firstname == $result['firstName']) && ($lastname == $result['lastName']) && ($gradyear == $result['gradYear']) ) )
		redirError("./passwordrecovery.php", "Incorrect Information Given.");
	
	//gets last 4 digits of username
	$lastdigits = substr($username, -4);
	
	$newpassword = $lastdigits . "-" . $gradyear;
	$passwordHash = password_hash($newpassword, PASSWORD_DEFAULT);
	
	/* Following Code can be used for stack-tracing
	$arr = get_defined_vars();
	print_r($arr);
	exit();
	*/
	
	$conn->query("UPDATE users SET hash = '{$conn->real_escape_string($passwordHash)}' WHERE username = '{$conn->real_escape_string($username)}'");

    redirSuccess("./login.php", "Password has been resetted.");
}

// hide the menu 
$hideMenu = true;
// include the header
include 'header.php';
// if the user loged in redirect to index.php
if($loggedIn)
	redir("./index.php");

?>

<br>
<div id="login-content" class="content-pane form vert-align">
	<h2>PASSWORD RECOVERY</h2>
	<form method="post">
		<br>
		<input name="username" type="text" placeholder="Username">
		<br><br>
		<input name="gradyear" type="text" placeholder="Graduation Year">
		<br><br>
		<input name="firstname" type="text" placeholder="First Name">
		<br><br>
		<input name="lastname" type="text" placeholder="Last Name">
		<br><br>
		<small>Your resetted password should be your four digits,<br>then a dash, then your graduation year: "9999-2019"</small>
		<br><br>
		<input name="submit" type="submit" value="Reset Password">
	</form>
</div>

<?php include 'footer.php' ?>