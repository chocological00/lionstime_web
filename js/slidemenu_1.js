function registerSlideMenu() {
	$('#slidemenu-container').click(function() {
		$('#slidemenu-container').css({
			bottom: '0'
		});
	});

	document.addEventListener('click', function(event) {
		var isClickInside = $('#slidemenu-container').get(0).contains(event.target);
		
		if (!isClickInside) {
			$('#slidemenu-container').css({
				bottom: '-204px'
			});
		}
	});
}

function slideMenuEntrance() {
	$("#slidemenu-container").slideDown();
}