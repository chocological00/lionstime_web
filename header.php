<?php

include 'sql.php';
include 'util.php';

$loggedIn = checkLoggedIn();

//checks if site is using ssl
if(!isSecure())
{
    header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
    exit();
}

if($loggedIn) {
	$result = $conn->query("SELECT * FROM `users` WHERE `id`=".$_SESSION['userid'])->fetch_assoc();
	
	$username = $result['username'];
	$firstName = $result['firstName'];
	$lastName = $result['lastName'];
	$email = $result['email'];
	$gradYear = $result['gradYear'];
}

$mobile = isMobile();
$hideMenu = isset($hideMenu) ? $hideMenu : false;

$config = json_decode(file_get_contents("data/CONFIG.json"), true);

?>

<!DOCTYPE html>
<html>
<head>
    <!-- We're glad that you're loking at this!
        If you're interested in being a developer of Lion's Time website
        Please email to dkim8420@icloud.com!
        Currently looking for: Front-end developer (CSS (w/ LESS or SASS) and HTML)
        -->
	<title><?php echo $config['Title']." ".date("Y") ?></title>
	
	<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
	<META HTTP-EQUIV="EXPIRES" CONTENT="Mon, 22 Jul 2002 11:12:01 GMT">

	<link href="/css/reset.css" rel="stylesheet" type="text/css">
	
	<!-- Conventional stylesheets, use CSS3 variables
	<link href="/css/style.css" rel="stylesheet" type="text/css">
	<link href="/css/menu-style.css" rel="stylesheet" type="text/css">
	
	For cross-browser support, using PHP variables instead:
	-->
	<?php 
	include './css/style.php';
	include './css/menu-style.php';
	?>
	
	<link rel="stylesheet" type="text/css" href="/css/Hover-master/css/hover.css">
	<link rel="stylesheet" type="text/css" href="/css/Font-Awesome/css/font-awesome.css">
	<link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
	<script src="/js/jquery.min.js"></script>
	
	<!-- Favicon stuff -->
	<link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" href="/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="/favicon/manifest.json">
	<link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#2196f3">
	<link rel="shortcut icon" href="/favicon/favicon.ico">
	<meta name="msapplication-config" content="/favicon/browserconfig.xml">
	<meta name="theme-color" content="#1976d2">
	
	<?php
	// Include different styles if they're on mobile
	if($mobile) { ?>
		<!--  Not using conventional stylesheets <link href="/css/mobile-style.css" rel="stylesheet" type="text/css"> -->
		<?php include './css/mobile-style.php'; ?>
		<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php } ?>
</head>

<body>
    <!-- We're glad that you're loking at this!
        If you're interested in being a developer of Lion's Time website
        Please email to dkim8420@icloud.com!
        Currently looking for: Front-end developer (CSS (w/ LESS or SASS) and HTML)
        -->
	<div id="header">
		<table id="header-table">
			<tr>
				<td>
					<a id="logo-link" href="/index.php"><img id="logo" src="/img/logo_title.png"></a>
				</td>
			</tr>
		</table>
		<?php
		// Check if the menu is supposed to be showing
		if(!$hideMenu) {
		?>
		
		
		<!-- Bottom slide menu -->
		<div id="slidemenu-container">
			<i class="fa fa-chevron-up"></i>
			<br>
			<table>
				<tr>
					<td><a href="/signup.php"><i class="fa fa-edit"></i></a></td>
					<td><a href="/signup.php"><span>Sign up</span></a></td>
				</tr>
				<tr>
					<td><a href="/passes.php"><i class="fa fa-ticket"></i></a></td>
					<td><a href="/passes.php"><span>Passes</span></a></td>
				</tr>
				<tr>
					<td><a href="/settings.php"><i class="fa fa-cogs"></i></a></td>
					<td><a href="/settings.php"><span>Settings</span></a></td>
				</tr>
				<tr>
					<td><a href="/logout.php"><i class="fa fa-sign-out"></i></a></td>
					<td><a href="/logout.php"><span>Logout</span></a></td>
				</tr>
			</table>
		</div>
		
		<?php } ?>
		
		<!-- Info bar -->
		<div id="info-bar">Error</div>
	</div>
	
	<script src="/js/slidemenu_1.js"></script>
	<script src="/js/transitions.js"></script>
	<script src="/js/init_1.js"></script>