<?php

$hideMenu = true;
include 'header.php';

?>

<br>
<div id="about-us" class="content-pane">
	<h1>Team 3D</h1>
	<br>
	<div class="card" id="zech" style="width: 350px; max-width: 350px;">
		<h2>Zach Sents</h2>
		<br>
		<br>
		Senior, Lead Programmer
		<br>
		<br>
		<table style="text-align: left; width: 350px; max-width: 350px">
			<tr>
				<td>
					<span>Email: </span>
				</td>
				<td>
					<span><!-- add later --></span>
				</td>
			</tr>
			<tr>
				<td>
					<span>Gitlab Profile: </span>
				</td>
				<td>
					<span><a href="https://gitlab.com/zachsents">@zechsents</a></span>
				</td>
			</tr>
		</table>
	</div>
	<br>
	<div class="card" id="david" style="width: 350px; max-width: 350px;">
		<h2>David "DW" Kim</h2>
		<br>
		<br>
		Junior, Server/DB Manager <br>Frontend and Backend
		<br>
		<br>
		<table style="text-align: left; width: 350px; max-width: 350px">
			<tr>
				<td>
					<span>Email: </span>
				</td>
				<td>
					<span>dkim8420@icloud.com</span>
				</td>
			</tr>
			<tr>
				<td>
					<span>Gitlab Profile: </span>
				</td>
				<td>
					<span><a href="https://gitlab.com/chocological00">@chocological00</a></span>
				</td>
			</tr>
			<tr>
				<td>
					<span>Github Profile: </span>
				</td>
				<td>
					<span><a href="https://github.com/chocological00">@chocological00</a></span>
				</td>
			</tr>
			<tr>
				<td>
					<span>Telegram: </span>
				</td>
				<td>
					<span><a href="https://t.me/chocological00">@chocological00</a></span>
				</td>
			</tr>
		</table>
	</div>
	<br>
	<div class="card" id="daniil" style="width: 350px; max-width: 350px;">
		<h2>Daniil Matseykanets</h2>
		<br>
		<br>
		Junior, Backend Developer
		<br>
		<br>
		<table style="text-align: left; width: 350px; max-width: 350px">
			<tr>
				<td>
					<span>Email: </span>
				</td>
				<td>
					<span><!-- add later --></span>
				</td>
			</tr>
			<tr>
				<td>
					<span>Gitlab Profile: </span>
				</td>
				<td>
					<span><a href="https://gitlab.com/RaginRussian">@RaginRussian</a></span>
				</td>
			</tr>
		</table>
	</div>
	<br>
	<div class="card" id="drew" style="width: 350px; max-width: 350px;">
		<h2>Drew Pleat</h2>
		<br>
		<br>
		Junior, SSL/Security Developer
		<br>
		<br>
		<table style="text-align: left; width: 350px; max-width: 350px">
			<tr>
				<td>
					<span>Email: </span>
				</td>
				<td>
					<span><!-- add later --></span>
				</td>
			</tr>
			<tr>
				<td>
					<span>Gitlab Profile: </span>
				</td>
				<td>
					<span><a href="https://gitlab.com/drp19">@drp19</a></span>
				</td>
			</tr>
		</table>
	</div>
	<br><br><br><br>
	<?php include 'footer.php' ?>