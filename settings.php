<?php

// If form has been submitted
if (isset ( $_POST ['submit-settings'] )) {
	
	include_once 'util.php';
	include_once 'sql.php';
	
	$userId = $conn->real_escape_string($_POST['userid']);
	
	// Update username
	if(isset($_POST['new-username'])) {
		$username = $conn->real_escape_string($_POST['new-username']);
		if($username != $_POST['username'] and $username != "") {
			
			// If username is taken
			$count = $conn->query ( "SELECT COUNT(*) AS `count` FROM `users` WHERE `username`='$username'" )->fetch_assoc () ['count'];
			if ($count > 0)
				redirError ( "./settings.php", "Oops, that username is taken." );
			
			$conn->query("UPDATE `users` SET `username`='$username' WHERE `id`=$userId");
		}
	}
	
	// Update email
	if(isset($_POST['new-email'])) {
		$email = $conn->real_escape_string($_POST['new-email']);
		if($email != $_POST['email'] and $email != "") {
			
			// If email is already used
			$count = $conn->query ( "SELECT COUNT(*) AS `count` FROM `users` WHERE `email`='$email'" )->fetch_assoc () ['count'];
			if ($count > 0)
				redirError ( "./settings.php", "It looks like there's already an account with that email." );
			
			// If email is valid
			if (! filter_var ( $email, FILTER_VALIDATE_EMAIL ))
				redirError ( "./settings.php", "Oops, that email address isn't right." );
				
			$conn->query("UPDATE `users` SET `email`='$email' WHERE `id`=$userId");
		}
	}

	// Update first name
	if(isset($_POST['new-firstName'])) {
		$firstName = $conn->real_escape_string($_POST['new-firstName']);
		if($firstName != $_POST['firstName'] and $firstName != "")
			$conn->query("UPDATE `users` SET `firstName`='$firstName' WHERE `id`=$userId");
	}
	
	// Update last name
	if(isset($_POST['new-lastName'])) {
		$lastName = $conn->real_escape_string($_POST['new-lastName']);
		if($lastName != $_POST['lastName'] and $lastName != "")
			$conn->query("UPDATE `users` SET `lastName`='$lastName' WHERE `id`=$userId");
	}
	
	// Update grad year
	if(isset($_POST['new-gradYear'])) {
		$gradYear = $conn->real_escape_string($_POST['new-gradYear']);
		if($gradYear != $_POST['gradYear'] and $gradYear != "")
			$conn->query("UPDATE `users` SET `gradYear`=$gradYear WHERE `id`=$userId");
	}
	
	// Update max students
	if(isset($_POST['new-maxStudents'])) {
		$maxStudents = $conn->real_escape_string($_POST['new-maxStudents']);
		if($maxStudents != $_POST['maxStudents'] and $maxStudents != "")
			$conn->query("UPDATE `users` SET `maxStudents`=$maxStudents WHERE `id`=$userId");
	}

	redir ( "./settings.php" );
}

include 'header.php';

if (! $loggedIn) {
	redir ( "./login.php" );
}

$minYear = intval ( date ( "Y" ) ) - 1;
$maxYear = $minYear + 6;

$userData = getUserData ( $_SESSION ['userid'] );


$isTeacher = $userData['teacher'] > 0;


$students = array();

if($isTeacher)
	$students = getStudents($userData['id']);


// if the from has been submitted to the server
if (isset($_POST['submit-password'])) {
    include_once 'util.php'; // include util.php
    include_once 'sql.php';  // include sql.php

    //if the field oldpass, newpass or newpass_retry are not recieved issue an invalid query parameters error
    if (!isset($_POST['oldpass']) or !isset($_POST['newpass']) or !isset($_POST['newpass-retry'])) {
        redirError("settings.php", "Invalid query parameters.");
    }

    $oldPassword = $_POST['oldpass'];
    $newPassword = $_POST['newpass'];
    $newPassword2 = $_POST['newpass-retry'];

    // checks if oldpassword, newpassword, newpassword2 are blank. If they are displays error.
    if ($oldPassword === "" or $newPassword === "" or $newPassword2 === "") {
        redirError("settings.php", "All fields must be filled in.");
    }

    // If the new password does not match retry display error
    if ($newPassword !== $newPassword2) {
        redirError("settings.php", "Passwords should match.");
    }

    $username = $_POST['username'];

    // requests the hash of the old password to be pulled from the server
    $query = $conn->query("SELECT hash FROM users WHERE username = '{$conn->real_escape_string($username)}'");
    // assigns the old hash to the variable passresult
    $result = $query->fetch_assoc();

    // if the old password has doesn't match the hash stored then display error
    if (!password_verify($oldPassword, $result['hash'])) {
        redirError("settings.php", "Old passwords don't match");
    }

    // hash the new password
    $newPasswordHash = password_hash($newPassword, PASSWORD_DEFAULT);

    // update the password in the SQL database
    $conn->query("UPDATE users SET hash = '{$conn->real_escape_string($newPasswordHash)}' WHERE username = '{$conn->real_escape_string($username)}'");

    redirSuccess('settings.php', 'Password has been changed.');
}

?>

<br>
<div id="settings-content" class="content-pane">
	<h1>SETTINGS</h1>
	<br>
	<div class="card" id="settings-aboutme">
		<h2><?php echo $userData['username']; ?></h2>
		<br>
		<br>
		<form method="post" class="colform">
			<table>
				<?php if($isTeacher) { ?>
				<tr>
					<td>
						<span>Username: </span>
					</td>
					<td>
						<input name="new-username" type="text"
							value=<?php echo "'".$userData['username']."'"; ?>>
					</td>
				</tr>
				<?php } ?>
				<tr>
					<td>
						<span>First Name: </span>
					</td>
					<td>
						<input name="new-firstName" type="text"
							value=<?php echo "'".$userData['firstName']."'"; ?>>
					</td>
				</tr>
				<tr>
					<td>
						<span>Last Name: </span>
					</td>
					<td>
						<input name="new-lastName" type="text"
							value=<?php echo "'".$userData['lastName']."'"; ?>>
					</td>
				</tr>
				<tr>
					<td>
						<span>Email Address: </span>
					</td>
					<td>
						<input name="new-email" type="email"
							value=<?php echo "'".$userData['email']."'"; ?>>
					</td>
				</tr>
				<?php if(!$isTeacher) { ?>
				<tr>
					<td>
						<span>Graduation Year: </span>
					</td>
					<td>
						<input name="new-gradYear" type="number"
							value=<?php echo "'".$userData['gradYear']."'"; ?>
							min=<?php echo "'$minYear'"; ?> max=<?php echo "'$maxYear'"; ?>>
					</td>
				</tr>
				<?php } ?>
				<?php if($isTeacher) { ?>
				<tr>
					<td>
						<span>Max Students: </span>
					</td>
					<td>
						<input name="new-maxStudents" type="number"
							value=<?php echo "'".$userData['maxStudents']."'"; ?>
							min='0'>
					</td>
				</tr>
				<?php } ?>
			</table>
			<br>
			<input name="submit-settings" type="submit" value="Save Settings">
			
			<input name="username" type="hidden" value=<?php echo "'".$userData['username']."'"; ?>>
			<input name="email" type="hidden" value=<?php echo "'".$userData['email']."'"; ?>>
			<input name="firstName" type="hidden" value=<?php echo "'".$userData['firstName']."'"; ?>>
			<input name="lastName" type="hidden" value=<?php echo "'".$userData['lastName']."'"; ?>>
			<input name="gradYear" type="hidden" value=<?php echo "'".$userData['gradYear']."'"; ?>>
			<input name="userid" type="hidden" value=<?php echo "'".$_SESSION['userid']."'"; ?>>
			<input name="maxStudents" type="hidden" value=<?php echo "'".$_SESSION['maxStudents']."'"; ?>>
		</form>
	</div>
	
	<?php if($isTeacher) { ?>
	
	<br>
	<div class="card" id="settings-students">
		<h2>My Students</h2>
		<p><?php echo sizeof($students)." student".(sizeof($students) == 1 ? "" : "s"); ?></p>
		<br>
		<ul>
			<?php 
			
			foreach($students as $student) {
				$name = getUserData($student['studentId']);
				echo "<li>".$name['firstName']." ".$name['lastName']."</li>";
			}
			
			?>
		</ul>
	</div>
	
	<?php } ?>
    
    <br>
	

    <div class="card" id="settings-change-password">
		<h2>Change Password</h2>
		<br>
		<br>
        <form method="post" class="colform">
        	<table>
				<tr>
					<td>
						<span>Old Password: </span>
					</td>
					<td>
						<input name="oldpass" type="password">

					</td>
				</tr>
				<tr>
					<td>
						<span>New Password: </span>
					</td>
					<td>
						<input name="newpass" type="password">
					</td>
				</tr>
                <tr>
					<td>
						<span>Retype New Password: </span>
					</td>
					<td>
						<input name="newpass-retry" type="password">
					</td>
				</tr>
			</table>
            <br>
			<input name="username" type="hidden" value=<?php echo "'".$userData['username']."'"; ?>>
			<input name="submit-password" type="submit" value="Change Password">
        </form>
	</div> 

	
    <br><br><br><br>
</div>

<?php include 'footer.php' ?>